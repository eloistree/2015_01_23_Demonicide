﻿using UnityEngine;
using System.Collections;

public class GodInputToBlocks : MonoBehaviour {

    public TeamManager gameManager;
    public PlayerKillerBlock blockLeft;
    public PlayerKillerBlock blockRight;



    void Update() {

        GodInput input = gameManager.god;
        if (gameManager == null|| blockLeft ==null  || blockRight==null || input==null) return;

        blockLeft.SetDirection(input.BlockLeftHand);
        blockRight.SetDirection(input.BlockRightHand);
    }
}
