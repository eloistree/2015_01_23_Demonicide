﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {
    int pos = 0;
    public GameObject[] screens;
    ControllerToPlayerInfo ctrlInfo;
	// Use this for initialization
	void Start () {
        ctrlInfo = GetComponent<ControllerToPlayerInfo>();
        ctrlInfo.onStartPressed += pressed;
	}
    void pressed()
    {
        screens[pos].SetActive(false);
        pos++;
        if (pos == screens.Length)
        {
            Application.LoadLevel(1);
            return;
        }
        screens[pos].SetActive(true);    
    }
	
	// Update is called once per frame
	void Update () {
	}
}
