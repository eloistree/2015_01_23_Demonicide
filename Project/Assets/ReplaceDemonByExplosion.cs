﻿using UnityEngine;
using System.Collections;

public class ReplaceDemonByExplosion : MonoBehaviour {


    public GameObject explosionPrefab;
    public Transform demonModelToHide;
    public Transform [] otherObjectToHide;


    public void Explode() {

        MultiplePooled.GetPooledObjectByName("BigExplosion", demonModelToHide);
        
        demonModelToHide.gameObject.SetActive(false);
        foreach (Transform obj in otherObjectToHide)
            obj.gameObject.SetActive(false);

        Destroy(this);
        return;
    }

}
