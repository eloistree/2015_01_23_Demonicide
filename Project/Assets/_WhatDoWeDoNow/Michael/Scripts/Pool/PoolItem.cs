﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolItem : MonoBehaviour {

    public enum PoolType { Recycle, PoolGrow, limited }

    public GameObject GameObjectPool;
    public int PoolSize = 20;
    public PoolType type = PoolType.Recycle;
    
    [HideInInspector]
    public List<GameObject> Pool;
    
    void Awake()
    {
        Pool = new List<GameObject>();
    }
}
