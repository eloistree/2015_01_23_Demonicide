﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;

public class MultiplePooled : MonoBehaviour
{

    static public MultiplePooled instance;
    static private Dictionary<string, PoolItem> P;

    void Awake()
    {
        if (instance != null) return;
        instance = this;
        P = new Dictionary<string, PoolItem>();
    }

    void Start()
    {

        PoolItem[] items = transform.GetComponentsInChildren<PoolItem>();

        foreach (PoolItem item in items)
        {
            item.transform.parent = transform;
            item.name = item.gameObject.name;
            item.Pool = new List<GameObject>();

            for (int i = 0; i < item.PoolSize; i++)
            {
                GameObject obj = Instantiate(item.GameObjectPool) as GameObject;
                obj.transform.name = item.name;
                obj.transform.parent = item.transform;
                obj.SetActive(false);
                item.Pool.Add(obj);
            }
            P.Add(item.name, item);
        }
    }

    static public GameObject GetPooledObjectByName(string name)
    {
        GameObject GO = null;
        PoolItem pi = P[name];

        if (pi != null)
        {
            for (int i = 0; i < pi.Pool.Count - 1; i++)
            {
                if (!pi.Pool[i].activeInHierarchy)
                {
                    return pi.Pool[i];
                }
            }

            switch (pi.type)
            {
                case PoolItem.PoolType.PoolGrow:
                    GO = Instantiate(pi.GameObjectPool) as GameObject;
                    GO.transform.parent = pi.transform;
                    pi.Pool.Add(GO);
                    break;

                case PoolItem.PoolType.Recycle:
                    GO = pi.Pool[0];
                    pi.Pool.RemoveAt(0);
                    pi.Pool.Insert(pi.PoolSize - 1, GO);
                    break;
            }
        }

        return GO;

    }

    static public GameObject GetPooledObjectByName(string name, Transform T, bool Activate = true)
    {
        GameObject GO = GetPooledObjectByName(name);
        if (GO != null)
        {
            GO.transform.position = T.position;
            GO.transform.rotation = T.rotation;
            GO.SetActive(Activate);
        }
        return GO;
    }
    static public GameObject GetPooledObjectByName(string name, Vector3 T, bool Activate = true)
    {
        GameObject GO = GetPooledObjectByName(name);
        if (GO != null)
        {
            GO.transform.position = T;
            GO.SetActive(Activate);
        }
        return GO;
    }

    static public GameObject GetPooledObject(GameObject PooledObject)
    {
        GameObject GO = GetPooledObjectByName(PooledObject.name);
        return GO;
    }

    static public GameObject GetPooledObject(GameObject PooledObject, Transform T, bool Activate = true)
    {
        GameObject GO = GetPooledObjectByName(PooledObject.name, T, Activate);
        return GO;
    }

    /// <summary>
    /// Add child pooled item to the Pool system.
    /// This will not override existing pool of the same prefab.
    /// </summary>
    /// <param name="prefab">The objet to pool</param>
    /// <param name="poolSize">The maximum number of items in pool</param>
    /// <param name="poolType">Limited / Grow / Recycle</param>
    static public void AddPrefabToPool(GameObject prefab, int poolSize = 0, PoolItem.PoolType poolType = PoolItem.PoolType.Recycle)
    {
        GameObject GO = Toolbox.FindGameObject(instance.transform, prefab.name);
        GO.transform.parent = instance.gameObject.transform;
        PoolItem PI = GO.GetComponent<PoolItem>();
        if (PI == null)
        {
            PI = GO.AddComponent<PoolItem>();
            PI.GameObjectPool = prefab;
            PI.type = poolType;
            if (poolSize > 0) PI.PoolSize = poolSize;
        }
    }
}
