﻿using UnityEngine;
using System.Collections;

public class DisableThisEvent : MonoBehaviour {

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
