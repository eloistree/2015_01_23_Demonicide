﻿using UnityEngine;
using System.Collections;

public class DisableThisTimed : MonoBehaviour {

    public float DisableAfter = 2.0f;
    private float lastCheck = -1f;

    void FixedUpdate()
    {
        if (lastCheck == -1f) lastCheck = Time.fixedTime;
        if (Time.fixedTime > lastCheck + DisableAfter)
        {
            lastCheck = -1f;
            gameObject.SetActive(false);
        }
    }
}
