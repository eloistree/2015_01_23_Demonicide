﻿using UnityEngine;
using System.Collections;

public class DestroyThisTimed : MonoBehaviour {

    public float destroyTime;
	
    void Start () {
        Destroy(gameObject, destroyTime);
	}

}
