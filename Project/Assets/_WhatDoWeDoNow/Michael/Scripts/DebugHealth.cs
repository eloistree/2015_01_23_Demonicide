﻿using UnityEngine;
using System.Collections;

public class DebugHealth : MonoBehaviour {

    public int damage = 10;
    private Life life;

    public void Awake()
    {
        life = GetComponent<Life>();
    }

    public void AddToLife(int health)
    {
        life.Health += health;
        Debug.Log("["+name + "] health = "+ life.Health);
    }

    public void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0)) AddToLife(-damage);
        if (Input.GetMouseButtonDown(1)) AddToLife(damage);
        if (Input.GetMouseButtonDown(2)) AddToLife(life._maxHealth);
    }


}
