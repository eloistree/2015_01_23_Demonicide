﻿using UnityEngine;
using System.Collections;

public class SpawnBlock : MonoBehaviour {

    public void Spawn()
    {
        MultiplePooled.GetPooledObjectByName("Brick", this.transform);
        gameObject.SetActive(false);
    }
}
