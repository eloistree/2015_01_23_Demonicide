﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Brick : Life {

    private SpriteRenderer sprite;
    public Sprite[] sprites;
    public Sprite idleSprite;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }
    void OnEnable()
    {
        this._health = 100;
        sprite.sprite = idleSprite;
    }
    
    public override void HasBeenDammaged(int oldLife, int newLife)
    {
        if (newLife == 0)
        {
            OnDestruction();
        }
        else
        {
            SetVisualDamageState();
        }
    }

    public override void HasBeenHeal(int oldLife, int newLife)
    {
        SetVisualDamageState();
    }

    private void Heal(Life life, int oldLife, int newLife)
    {
        SetVisualDamageState();
    }

    private void OnDestruction()
    {
        gameObject.SetActive(false);
        MultiplePooled.GetPooledObjectByName("BrickExplosion", this.transform);
    }

    private void SetVisualDamageState()
    {
        int spriteNumber = (Health * (sprites.Length-1)) / 100;
        sprite.sprite= sprites[spriteNumber];
    }
}
