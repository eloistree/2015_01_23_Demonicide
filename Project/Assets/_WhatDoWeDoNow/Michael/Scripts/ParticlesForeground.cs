﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(ParticleSystem))]
public class ParticlesForeground : MonoBehaviour
{

    public int renderQueue = 3000;
    public string sortingLayer = "Foreground";

    private Material mMat;

    void Start()
    {
        particleSystem.renderer.sortingLayerName = sortingLayer;

        Renderer ren = renderer;

        if (ren == null)
        {
            ParticleSystem sys = GetComponent<ParticleSystem>();
            if (sys != null) ren = sys.renderer;
        }

        if (ren != null)
        {
            mMat = new Material(ren.sharedMaterial);
            mMat.renderQueue = renderQueue;
            ren.material = mMat;
        }
    }

    void OnDestroy() { if (mMat != null) Destroy(mMat); }
}