﻿using UnityEngine;
using System.Collections;

public class RotationPlan2D_Limited : MonoBehaviour {

    public Quaternion initialRotation;
    public float rightRotationMax = 40f;
    public float leftRotationMax = -40f;

    public float currentRotation = 0f;
    public float wantedRotation = 0f;

    public bool inverseRotAxies =true;
    public float moveAngleSpeed = 90f;

    public float adjustment = 90;

    public void SetAngle(float wantedRot) {

        wantedRotation   =wantedRot;
    }

	void Start () {
        initialRotation = this.transform.localRotation;
	}
	
	void Update () {

        wantedRotation = Mathf.Clamp(wantedRotation, leftRotationMax, rightRotationMax)+adjustment;

        currentRotation = Mathf.MoveTowards(currentRotation, wantedRotation, Time.deltaTime * moveAngleSpeed);
        this.transform.localRotation = initialRotation * Quaternion.Euler(0, 0, currentRotation * (inverseRotAxies?-1f:1f));
	}
}
