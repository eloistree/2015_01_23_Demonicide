﻿using UnityEngine;
using System.Collections;
using System;

public class GodInput : MonoBehaviour
{

   

    public bool _meteorLoading;


    public bool MeteorLoading
    {
        get { return _meteorLoading; }
        set
        {
            //try
            //{
                if (_meteorLoading && !value)
                {

                    if (onMeteor != null)
                        onMeteor(MeteorLoadingTime);
                    MeteorLoadingTime = 0f;
                }
            //}
            //catch (Exception e) { Debug.LogException(e, this.gameObject); }
            _meteorLoading = value;
        }
    }
    public float MeteorLoadingTime;


    public bool _isUsingShield;
    public bool IsUsingShield
    {
        get { return _isUsingShield; }
        set
        {
            if (_isUsingShield && !value)
            {
                if (onStartShield != null)
                    onStartShield(IsUsingShieldTime);
                IsUsingShieldTime = 0f;
            }
            _isUsingShield = value;
        }
    }
    public float IsUsingShieldTime;


    public delegate void Meteor(float loadingTime);
    public Meteor onMeteor;

    public delegate void StartShield(float loadingTime);
    public StartShield onStartShield;

    public delegate void StopShield(float loadingTime);
    public StopShield onStopShield;


    public bool withDebug =false;
    public Vector3 BlockLeftHand;
    public Vector3 BlockRightHand;

    void Update() {
        if (MeteorLoading)
            MeteorLoadingTime += Time.deltaTime;
        if (IsUsingShield)
            IsUsingShieldTime += Time.deltaTime;
        if (withDebug)
        {
         
            Debug.Log(string.Format("God: shield {0} meteor {1} ", MeteorLoadingTime, IsUsingShieldTime));
        }
    }
}
