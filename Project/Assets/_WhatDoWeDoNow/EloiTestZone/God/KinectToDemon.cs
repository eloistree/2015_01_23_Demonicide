﻿using UnityEngine;
using System.Collections;

public class KinectToDemon : MonoBehaviour {



    public Transform root;
    public Transform spineBase;
    public Transform neck;
    public Transform handLeft;
    public Transform handRight;

    public Transform demonRootModel;
    public Transform demonHeadModel;

    public RotationPlan2D_Limited bodyAngle;

    public RotationPlan2D_Limited armLeft;
    public RotationPlan2D_Limited armRight;


	
	void Update () {

        Vector2 rootVar = root.position;
        Vector2 spineVar = spineBase.position;
        Vector2 neckVar = neck.position;
        Vector2 handLeftVar = handLeft.position;
        Vector2 handRightVar = handRight.position;
        
        Vector2 from = rootVar -spineVar;
        Vector2 to = neckVar -spineVar;

        bool dirIsLeft = to.x < 0;

        float angleBodyDirection = 180f - Vector2.Angle(from, to);
        bodyAngle.SetAngle(angleBodyDirection * (dirIsLeft ? -1f : 1f));


        from = spineVar - neckVar;
        to = handLeftVar - neckVar;

        float angleHandLeft =  Vector2.Angle(from, to)-90f;
        armLeft.SetAngle(angleHandLeft);

        from = spineVar - neckVar;
        to = handRightVar - neckVar;    

        float angleHandRight =  Vector2.Angle(from, to) -90;
        armRight.SetAngle(angleHandRight);


        demonHeadModel.rotation = root.rotation;


	}
}
