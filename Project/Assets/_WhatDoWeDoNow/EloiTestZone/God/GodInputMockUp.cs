﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GodInput))]
public class GodInputMockUp : MonoBehaviour {

    private GodInput godInput;

    public KeyCode axeTeamOne = KeyCode.Keypad7;
    public KeyCode bowTeamOne = KeyCode.Keypad4;
    public KeyCode swordTeamOne = KeyCode.Keypad1;

    public KeyCode axeTeamTwo= KeyCode.Keypad9;
    public KeyCode bowTeamTwo = KeyCode.Keypad6;
    public KeyCode swordTeamTwo = KeyCode.Keypad3;


    public KeyCode loadMeteor = KeyCode.Keypad8;
    public KeyCode useShield = KeyCode.Keypad5;


    void Start() { 
        godInput = GetComponent<GodInput>() as GodInput;
    }
	void Update () {


        //godInput.IsPickAxeTeamOneActive = Input.GetKey(axeTeamOne);
        //godInput.IsBowTeamOneActive = Input.GetKey(bowTeamOne);
        //godInput.IsSwordTeamOneActive = true;


        //godInput.IsPickAxeTeamTwoActive = Input.GetKey(axeTeamTwo);
        //godInput.IsBowTeamTwoActive = Input.GetKey(bowTeamTwo);
        //godInput.IsSwordTeamTwoActive = true;


        godInput.IsUsingShield = Input.GetKey(useShield);
        godInput.MeteorLoading = Input.GetKey(loadMeteor);

	}
}
