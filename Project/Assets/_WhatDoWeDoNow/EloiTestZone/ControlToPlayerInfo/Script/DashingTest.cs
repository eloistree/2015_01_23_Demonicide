﻿using UnityEngine;
using System.Collections;

public class DashingTest : MonoBehaviour {

    public ControllerToPlayerInfo playerInfo;
    // Use this for initialization
    void Start()
    {
        playerInfo = GetComponent<ControllerToPlayerInfo>() as ControllerToPlayerInfo;
        if (playerInfo == null) { Destroy(this); return; }

        playerInfo.onPlayerStartDashing += PlayerDash;
    }


    void OnDestroy()
    {
        playerInfo.onPlayerStopDashing -= PlayerDash;
    }

    private void PlayerDash(int player)
    {
        Debug.Log(string.Format("Player {0} dash", player));
    }
}
