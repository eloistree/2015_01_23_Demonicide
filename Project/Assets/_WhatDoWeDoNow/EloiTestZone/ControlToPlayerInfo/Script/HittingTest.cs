﻿using UnityEngine;
using System.Collections;

public class HittingTest : MonoBehaviour {

    public ControllerToPlayerInfo playerInfo;
	// Use this for initialization
	void Start () {
        playerInfo = GetComponent<ControllerToPlayerInfo>() as ControllerToPlayerInfo;
        if (playerInfo == null) { Destroy(this); return; }

        playerInfo.onPlayerStartHitting += PlayerHit;
	}

   
    void OnDestroy() {

        playerInfo.onPlayerStartHitting -= PlayerHit;
    }

    private void PlayerHit(int player)
    {
        Debug.Log(string.Format("Player {0} hit",player));
    }

}
