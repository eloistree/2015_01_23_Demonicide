﻿using UnityEngine;
using System.Collections;

public class MovingTest : MonoBehaviour {

    public ControllerToPlayerInfo playerInfo;
    void Start()
    {
        playerInfo = GetComponent<ControllerToPlayerInfo>() as ControllerToPlayerInfo;
        if (playerInfo == null) { Destroy(this); return; }

        playerInfo.onPlayerStartMoving += PlayerStartMoving;
        playerInfo.onPlayerStopMoving += PlayerStopMoving;
    }

    private void PlayerStopMoving(int player, Vector2 lastMoveDirectionAtStop)
    {
        Debug.Log(string.Format("Player {0} stop moving with this  last direction {1} ", player,lastMoveDirectionAtStop));
    }

    private void PlayerStartMoving(int player, Vector2 startMoveDirection)
    {
        Debug.Log(string.Format("Player {0} start moving toward {1} ", player, startMoveDirection));
    }


    void OnDestroy()
    {

        playerInfo.onPlayerStartMoving -= PlayerStartMoving;
        playerInfo.onPlayerStopMoving -= PlayerStopMoving;
    }

    
}
