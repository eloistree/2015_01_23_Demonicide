﻿using UnityEngine;
using System.Collections;

public class ControllerToPlayerInfo : MonoBehaviour
{

    public bool IsPlayerHitting(int player)
    {
        switch (player)
        {
            case 1:
                return IsPlayer1Hitting;
            case 2:
                return IsPlayer2Hitting;
            case 3:
                return IsPlayer3Hitting;
            case 4:
                return IsPlayer4Hitting;
        }
        return false;
    }
    internal bool IsPlayerBowing(int player)
    {

        switch (player)
        {
            case 1:
                return IsButtonActiveBy(X360.Player.P1, _p1BowButtonChosen);
            case 2:
                return IsButtonActiveBy(X360.Player.P2, _p2BowButtonChosen);
            case 3:
                return IsButtonActiveBy(X360.Player.P3, _p3BowButtonChosen);
            case 4:
                return IsButtonActiveBy(X360.Player.P4, _p4BowButtonChosen);
         }
                return false;
    }



    internal bool IsPlayerPickaxe(int player)
    {

        switch (player)
        {
            case 1:
                return IsButtonActiveBy(X360.Player.P1, _p1PickAxeButtonChosen);
            case 2:
                return IsButtonActiveBy(X360.Player.P2, _p2PickAxeButtonChosen);
            case 3:
                return IsButtonActiveBy(X360.Player.P3, _p3PickAxeButtonChosen);
            case 4:
                return IsButtonActiveBy(X360.Player.P4, _p4PickAxeButtonChosen);
        }
        return false;
    }

    private bool IsButtonActiveBy(X360.Player player, X360.ButtonType button)
    {
        return X360.IsActive(button, player);
    }
    public bool IsPlayer1Hitting
    {
        get { return _p1IsHitting; }
        private set
        {
            if (value != _p1IsHitting)
            {
                NotifyHitSwitch(X360.Player.P1, _p1IsHitting, value);

            }
            _p1IsHitting = value;
        }
    }

    public bool IsPlayer2Hitting
    {
        get { return _p2IsHitting; }
        private set
        {
            if (value != _p2IsHitting)
            {
                NotifyHitSwitch(X360.Player.P2, _p2IsHitting, value);

            }
            _p2IsHitting = value;
        }
    }
    public bool IsPlayer3Hitting
    {
        get { return _p3IsHitting; }
        private set
        {
            if (value != _p3IsHitting)
            {
                NotifyHitSwitch(X360.Player.P3, _p3IsHitting, value);

            }
            _p3IsHitting = value;
        }
    }
    public bool IsPlayer4Hitting
    {
        get { return _p4IsHitting; }
        private set
        {
            if (value != _p4IsHitting)
            {
                NotifyHitSwitch(X360.Player.P4, _p4IsHitting, value);

            }
            _p4IsHitting = value;
        }
    }



    public bool IsPlayerDashing(int player)
    {
        switch (player)
        {
            case 1:
                return IsPlayer1Dashing;
            case 2:
                return IsPlayer2Dashing;
            case 3:
                return IsPlayer3Dashing;
            case 4:
                return IsPlayer4Dashing;
            default:
                return IsPlayer1Dashing;
        }
    }

    public bool IsPlayer1Dashing
    {
        get { return _p1IsDashing; }
        private set
        {
            if (value != _p1IsDashing)
            {
                NotifyDashSwitch(X360.Player.P1, _p1IsDashing, value);

            }
            _p1IsDashing = value;
        }
    }
    public bool IsPlayer2Dashing
    {
        get { return _p2IsDashing; }
        private set
        {
            if (value != _p2IsDashing)
            {
                NotifyDashSwitch(X360.Player.P2, _p2IsDashing, value);

            }
            _p2IsDashing = value;
        }
    }
    public bool IsPlayer3Dashing
    {
        get { return _p3IsDashing; }
        private set
        {
            if (value != _p3IsDashing)
            {
                NotifyDashSwitch(X360.Player.P3, _p3IsDashing, value);

            }
            _p3IsDashing = value;
        }
    }
    public bool IsPlayer4Dashing
    {
        get { return _p4IsDashing; }
        private set
        {
            if (value != _p4IsDashing)
            {
                NotifyDashSwitch(X360.Player.P4, _p4IsDashing, value);

            }
            _p4IsDashing = value;
        }
    }


    public Vector2 PlayerDirection(int player)
    {
        switch (player)
        {
            case 1:
                return Player1Direction;
            case 2:
                return Player2Direction;
            case 3:
                return Player3Direction;
            case 4:
                return Player4Direction;
            default:
                return Player1Direction;
        }
    }

    public Vector2 Player1Direction
    {
        get { return _p1Vector; }
        private set
        {
            if (value != _p1Vector)
            {
                NotifyMoveChange(X360.Player.P1, _p1Vector, value);

            }
            _p1Vector = value;
        }
    }

    public Vector2 Player2Direction
    {
        get { return _p2Vector; }
        private set
        {
            if (value != _p2Vector)
            {
                NotifyMoveChange(X360.Player.P2, _p2Vector, value);

            }
            _p2Vector = value;
        }
    }

    public Vector2 Player3Direction
    {
        get { return _p3Vector; }
        private set
        {
            if (value != _p3Vector)
            {
                NotifyMoveChange(X360.Player.P3, _p3Vector, value);
            }
            _p3Vector = value;
        }
    }

    public Vector2 Player4Direction
    {
        get { return _p4Vector; }
        private set
        {
            if (value != _p4Vector)
            {
                NotifyMoveChange(X360.Player.P4, _p4Vector, value);

            }
            _p4Vector = value;
        }
    }




    public Vector2 _p1Vector;
    public bool _p1IsHitting;
    public bool _p1IsDashing;

    public Vector2 _p2Vector;
    public bool _p2IsHitting;
    public bool _p2IsDashing;

    public Vector2 _p3Vector;
    public bool _p3IsHitting;
    public bool _p3IsDashing;

    public Vector2 _p4Vector;
    public bool _p4IsHitting;
    public bool _p4IsDashing;



    public X360.ButtonType _p1AttackButtonChosen = X360.ButtonType.A;
    public X360.ButtonType _p1DashButtonChosen = X360.ButtonType.X;
    public X360.ButtonType _p1BowButtonChosen = X360.ButtonType.B;
    public X360.ButtonType _p1PickAxeButtonChosen = X360.ButtonType.Y;
   

    public X360.ButtonType _p2AttackButtonChosen = X360.ButtonType.A;
    public X360.ButtonType _p2DashButtonChosen = X360.ButtonType.X;
    public X360.ButtonType _p2BowButtonChosen = X360.ButtonType.B;
    public X360.ButtonType _p2PickAxeButtonChosen = X360.ButtonType.Y;
 
    public X360.ButtonType _p3AttackButtonChosen = X360.ButtonType.A;
    public X360.ButtonType _p3DashButtonChosen = X360.ButtonType.X;
    public X360.ButtonType _p3BowButtonChosen = X360.ButtonType.B;
    public X360.ButtonType _p3PickAxeButtonChosen = X360.ButtonType.Y;
 
    public X360.ButtonType _p4AttackButtonChosen = X360.ButtonType.A;
    public X360.ButtonType _p4DashButtonChosen = X360.ButtonType.X;
    public X360.ButtonType _p4BowButtonChosen = X360.ButtonType.B;
    public X360.ButtonType _p4PickAxeButtonChosen = X360.ButtonType.Y;


    void Update()
    {

        Player1Direction = IsUsingOneDirection(X360.Player.P1);
        Player2Direction = IsUsingOneDirection(X360.Player.P2);
        Player3Direction = IsUsingOneDirection(X360.Player.P3);
        Player4Direction = IsUsingOneDirection(X360.Player.P4);

        IsPlayer1Hitting = IsPlayerHitting(X360.Player.P1);
        IsPlayer2Hitting = IsPlayerHitting(X360.Player.P2);
        IsPlayer3Hitting = IsPlayerHitting(X360.Player.P3);
        IsPlayer4Hitting = IsPlayerHitting(X360.Player.P4);

        IsPlayer1Dashing = IsPlayerDashing(X360.Player.P1);
        IsPlayer2Dashing = IsPlayerDashing(X360.Player.P2);
        IsPlayer3Dashing = IsPlayerDashing(X360.Player.P3);
        IsPlayer4Dashing = IsPlayerDashing(X360.Player.P4);


        IsStartPressed = IsStartButton();


    }
    public delegate void PlayerStartPressed();
    public PlayerStartPressed onStartPressed;
    private bool _isStartPressed;
    public bool IsStartPressed
    {
        get { return _isStartPressed; }
        private set
        {
            if (value && !_isStartPressed)
            {
                onStartPressed();

            }
            _isStartPressed = value;
        }
    }
    
    public bool IsStartButton()
    {
        X360.ButtonType button = X360.ButtonType.Start;
        return X360.IsActive(button, X360.Player.All);
    }
    #region Process use in Update

    private bool IsPlayerDashing(X360.Player player)
    {
        X360.ButtonType button = X360.ButtonType.A;
        switch (player)
        {
            case X360.Player.P1: button = _p1DashButtonChosen;
                break;
            case X360.Player.P2: button = _p2DashButtonChosen;
                break;
            case X360.Player.P3: button = _p3DashButtonChosen;
                break;
            case X360.Player.P4: button = _p4DashButtonChosen;
                break;
        }
        return X360.IsActive(button, player);
    }
    private bool IsPlayerHitting(X360.Player player)
    {
        X360.ButtonType button = X360.ButtonType.B;
        switch (player)
        {
            case X360.Player.P1: button = _p1AttackButtonChosen;
                break;
            case X360.Player.P2: button = _p2AttackButtonChosen;
                break;
            case X360.Player.P3: button = _p3AttackButtonChosen;
                break;
            case X360.Player.P4: button = _p4AttackButtonChosen;
                break;
        }
        return X360.IsActive(button, player);
    }

    private Vector3 IsUsingOneDirection(X360.Player player)
    {
        Vector3 direction = Vector3.zero;
        direction = X360.GetDirection(X360.Direction.Left, player);
        if (direction != Vector3.zero)
            return direction;
        direction = X360.GetDirection(X360.Direction.Right, player);
        if (direction != Vector3.zero)
            return direction;
        direction = X360.GetDirection(X360.Direction.Arrow, player);
        if (direction != Vector3.zero)
            return direction;
        return direction;
    }

    #endregion

    #region Delegate and notification
    private void NotifyHitSwitch(X360.Player player, bool oldValue, bool newValue)
    {
        if (onPlayerStartHitting != null && !oldValue && newValue)
        {
            onPlayerStartHitting(PlayerConvertInt(player));
        }
        if (onPlayerStopHitting != null && oldValue && !newValue)
        {
            onPlayerStopHitting(PlayerConvertInt(player));
        }
    }
    private void NotifyMoveChange(X360.Player player, Vector2 oldValue, Vector2 newValue)
    {
        if (onPlayerStartMoving != null && oldValue == Vector2.zero && newValue != Vector2.zero)
        {
            onPlayerStartMoving(PlayerConvertInt(player), newValue);
        }
        if (onPlayerStopMoving != null && oldValue != Vector2.zero && newValue == Vector2.zero)
        {
            onPlayerStopMoving(PlayerConvertInt(player), oldValue);
        }

    }
    private void NotifyDashSwitch(X360.Player player, bool oldValue, bool newValue)
    {
        if (onPlayerStartDashing != null && !oldValue && newValue)
        {
            onPlayerStartDashing(PlayerConvertInt(player));
        }
        if (onPlayerStopDashing != null && oldValue && !newValue)
        {
            onPlayerStopDashing(PlayerConvertInt(player));
        }
    }

    /// <param name="player">1,2,3,4</param>
    public delegate void PlayerStartMoving(int player, Vector2 startMoveDirection);
    public PlayerStartMoving onPlayerStartMoving;

    /// <param name="player">1,2,3,4</param>
    public delegate void PlayerStopMoving(int player, Vector2 lastMoveDirectionAtStop);
    public PlayerStopMoving onPlayerStopMoving;


    /// <param name="player">1,2,3,4</param>
    public delegate void PlayerStartHitting(int player);
    public PlayerStartHitting onPlayerStartHitting;

    /// <param name="player">1,2,3,4</param>
    public delegate void PlayerStopHitting(int player);
    public PlayerStopHitting onPlayerStopHitting;


    /// <param name="player">1,2,3,4</param>
    public delegate void PlayerStartDashing(int player);
    public PlayerStartDashing onPlayerStartDashing;

    /// <param name="player">1,2,3,4</param>
    public delegate void PlayerStopDashing(int player);
    public PlayerStopDashing onPlayerStopDashing;

    #endregion



    private int PlayerConvertInt(X360.Player player)
    {
        switch (player)
        {
            case X360.Player.P1:
                return 1;
            case X360.Player.P2:
                return 2;
            case X360.Player.P3:
                return 3;
            case X360.Player.P4:
                return 4;
        }
        return -1;
    }


}
