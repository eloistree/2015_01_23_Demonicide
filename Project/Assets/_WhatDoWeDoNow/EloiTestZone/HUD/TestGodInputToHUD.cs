﻿using UnityEngine;
using System.Collections;

public class TestGodInputToHUD : MonoBehaviour {

    public GameDataToHUD hud;
    public GodInput input;

    void Update() { 

        PlayerAndBossHUD.WeaponItem weapon ;
        weapon = PlayerAndBossHUD.WeaponItem.Sword;
        //if (input.IsBowTeamOneActive) weapon = PlayerAndBossHUD.WeaponItem.Bow;
        //else if (input.IsPickAxeTeamOneActive) weapon = PlayerAndBossHUD.WeaponItem.Axe;
        hud.SetWeapon(0, weapon);
        hud.SetWeapon(2, weapon);

        weapon = PlayerAndBossHUD.WeaponItem.Sword;
        //if (input.IsBowTeamTwoActive) weapon = PlayerAndBossHUD.WeaponItem.Bow;
        //else if (input.IsPickAxeTeamTwoActive) weapon = PlayerAndBossHUD.WeaponItem.Axe;
        hud.SetWeapon(1, weapon);
        hud.SetWeapon(3, weapon);
    }

}
