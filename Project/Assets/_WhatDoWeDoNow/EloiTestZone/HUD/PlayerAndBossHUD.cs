﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerAndBossHUD : MonoBehaviour {

    /// <summary>
    ///  0 = TopLeft , 1 = TopRight, 2 = BotLeft, 3 BotRight
    /// </summary>

//    public Image[] playersLife;
    public UISlider[] playersLifeSlider;

    /// <summary>
    ///  0 = TopLeft , 1 = TopRight, 2 = BotLeft, 3 BotRight
    /// </summary>
//    public Image[] playersWeapon;

    //public Image bossLife;
    public UISlider bossLifeSlider;
    //public Image bossMana;
    public UISlider bossManaSlider;

//    public Text scoreTeam1;
//    public Text scoreTeam2;

    public UILabel scorePlayer1Label;
    public UILabel scorePlayer2Label;
    public UILabel scorePlayer3Label;
    public UILabel scorePlayer4Label;

    //public Text scorePlayer1;
    //public Text scorePlayer2;
    //public Text scorePlayer3;
    //public Text scorePlayer4;

    public enum WeaponItem{Bow, Sword, Axe}
    //public Sprite bow;
    //public Sprite sword;
    //public Sprite axe;
    //public UISprite weaponPlayer1;
    //public UISprite weaponPlayer2;
    //public UISprite weaponPlayer3;
    //public UISprite weaponPlayer4;


    /// <param name="player">0 1 2 3</param>
    /// <param name="pourcent"></param>
    public void SetPlayerLife(int player, float pourcent) 
    {
        playersLifeSlider[player].value = Mathf.Clamp(pourcent, 0f, 1f); ;
//        playersLife[player].fillAmount = Mathf.Clamp(pourcent, 0f, 1f);
    }
    public void SetWeaponTo(int player, WeaponItem weapon) 
    {
        //string spriteName = GetWeaponSpriteName(weapon);
        //switch(player)
        //{
        //    case 0:
        //        weaponPlayer1.spriteName = spriteName;
        //        break;
        //    case 1:
        //        weaponPlayer2.spriteName = spriteName;
        //        break;
        //    case 2:
        //        weaponPlayer3.spriteName = spriteName;
        //        break;
        //    case 3:
        //        weaponPlayer4.spriteName = spriteName;
        //        break;
        //}
    }

    public void SetBossLife(float pourcent)
    {
        //bossLife.fillAmount = Mathf.Clamp(pourcent, 0f, 1f);
        bossLifeSlider.value = Mathf.Clamp(pourcent, 0f, 1f);
    }
    public void SetBossMana(float pourcent)
    {
        //bossMana.fillAmount = Mathf.Clamp(pourcent, 0f, 1f);
        bossManaSlider.value = Mathf.Clamp(pourcent, 0f, 1f);
    }

    private string GetWeaponSpriteName(WeaponItem weapon)
    {
        switch (weapon)
        {
            case WeaponItem.Bow:
                return "arc";
            case WeaponItem.Axe:
                return "pioche";
            default:
                return "Epee";
        }
    }

    public void SetScore(int scoreTeam1, int scoreTeam2)
    {
        //this.scoreTeam1.text = "" + scoreTeam1;
        //this.scoreTeam2.text = "" + scoreTeam2;
    }
    public void SetScore(int[] score)
    {
        this.scorePlayer1Label.text = score[0].ToString();
        this.scorePlayer2Label.text = score[2].ToString();
        this.scorePlayer3Label.text = score[1].ToString();
        this.scorePlayer4Label.text = score[3].ToString();

        //this.scorePlayer1.text = "" + score[0];
        //this.scorePlayer2.text = "" + score[1];
        //this.scorePlayer3.text = "" + score[2];
        //this.scorePlayer4.text = "" + score[3];
    }
}
