﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerAndBossHUD))]
public class GameDataToHUD : MonoBehaviour
{
    public PlayerAndBossHUD _hud;

    public float[] _playerLife= new float[]{1f,1f,1f,1f};
    public float _bossMana=1f;
    public float _bossLife=1f;

    public PlayerAndBossHUD.WeaponItem[] _playerWeapon = new PlayerAndBossHUD.WeaponItem[] { PlayerAndBossHUD.WeaponItem.Sword, PlayerAndBossHUD.WeaponItem.Sword, PlayerAndBossHUD.WeaponItem.Sword, PlayerAndBossHUD.WeaponItem.Sword};
    public int _scoreTeam1;
    public int _scoreTeam2;
    public int[] _score;

    void Start() {
        _hud = GetComponent<PlayerAndBossHUD>() as PlayerAndBossHUD;
    }

    /// <param name="player">0,1,2,3</param>
    public void SetPlayerLife(int index, float pctLife)
    {
        if (_playerLife[index] != pctLife)
        { _playerLife[index] = pctLife;
        _hud.SetPlayerLife(index, pctLife);
        }
    }
    /// <param name="player">0,1,2,3</param>
    public void SetPlayersScore(int[] scores){
        _score = scores;
        _hud.SetScore(scores);
    }
    public void SetWeapon(int index, PlayerAndBossHUD.WeaponItem weapon) {
        if (_playerWeapon[index] != weapon)
        {
            _playerWeapon[index] = weapon;
            _hud.SetWeaponTo(index, weapon);
        }
    }
    public void SetBossLife(float pctLife)
    {
        if (_bossLife != pctLife)
        {
            _bossLife = pctLife;
            _hud.SetBossLife(pctLife);
        }
    }
    public void SetBossMana(float pctMana)
    {
        if (_bossMana != pctMana)
        {
            _bossMana = pctMana;
            _hud.SetBossMana (pctMana);
        }
    }
    void Update()
    {
    #if UNITY_EDITOR
            int i = 0;
            foreach (float pctLife in _playerLife)
            {
                SetPlayerLife(i, _playerLife[i]);
                i++;
            }
            i = 0;
            foreach (PlayerAndBossHUD.WeaponItem weapon in _playerWeapon)
            {
                _hud.SetWeaponTo(i, _playerWeapon[i]);
                i++;
            }

            _hud.SetBossLife(_bossLife);
            _hud.SetBossMana(_bossMana);

            _hud.SetScore(_scoreTeam1, _scoreTeam2);
    #endif
    }
}