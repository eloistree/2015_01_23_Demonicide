﻿using UnityEngine;
using System.Collections;

public class GodDeathObserver : MonoBehaviour {

    public TeamManager gameManager;
    public ReplaceDemonByExplosion replaceBy;

    
	void Update () {
        if (gameManager != null && replaceBy != null) 
        {

            if (gameManager.GodHp <= 0f) replaceBy.Explode();
        }
	
	}
}
