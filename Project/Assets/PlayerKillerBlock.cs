﻿using UnityEngine;
using System.Collections;

public class PlayerKillerBlock : MonoBehaviour {



    public float speed =1f;
    public Vector3 direction;


    public void SetDirection(Vector3 direction)
    {

        this.direction = direction;
    }

    void Update() 
    {
        this.rigidbody2D.velocity = direction * speed;
    }


    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name.Contains("Player")) {
             Life life =   coll.gameObject.GetComponent<Life>() as Life;
             if (life != null) {
                 life.Health = 0;
             }
        
        }


    }
}
