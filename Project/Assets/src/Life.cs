﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Life : MonoBehaviour
{
    [Range(0, 100)]
    public int _health = 100;
    [Range(0, 100)]
    public int _maxHealth = 100;

    public bool isInvincible = false;

    public int Health {
        get { return _health; }
        set {
            if (isInvincible != true)
            {
                value = Mathf.Clamp(value, 0, _maxHealth);

                if (value != _health)
                {
                    NotifyHealthChange(_health, value);
                }
                _health = value;
            }
        }
    }

    private void NotifyHealthChange(int oldLife,int newLife)
    {

        if (oldLife != newLife && oldLife>newLife)
        {
            HasBeenDammaged(oldLife, newLife);
            if (onTakingDommage != null)
                onTakingDommage(this, oldLife, newLife);
        }

        if (oldLife != newLife && oldLife < newLife)
        {
            HasBeenHeal(oldLife, newLife);
            if (onHeal != null)
                onHeal(this, oldLife, newLife);
        }
    }

    public virtual void HasBeenHeal(int oldLife, int newLife) { }
    public virtual void HasBeenDammaged(int oldLife, int newLife) { }

    public delegate void OnTakingDommage(Life life, int oldLife, int newLife);
    public static OnTakingDommage onTakingDommage;
    public delegate void OnHeal(Life life, int oldLife, int newLife);
    public static OnHeal onHeal;
}
