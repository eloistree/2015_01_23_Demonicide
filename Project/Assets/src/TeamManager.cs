﻿using UnityEngine;
using System.Collections;

public class TeamManager : MonoBehaviour {
    public PlayerCtrl[] Players;
    public PlayerCtrl[] TeamOne;
    public PlayerCtrl[] TeamTwo;
    public GameDataToHUD hud;
    public GodInput god;
    public int shift = 0;
    public float meteorManaCost = 10;
    public float shieldManaCost = 5;
    public float GodRegenMana = 5;
    public GameObject[] blockList;

    public Vector2 meteorOrigin;
    public Vector2 meteorGodPosition;
    public int meteorRainRadius = 8;

    int[] playerScore;
    
    public AudioClip[] clips;
    AudioSource audio;

    public float GodMaxHp = 100;
    public float GodMaxMana = 100;
    public float GodHp = 100;
    public float GodMana = 100;
    float pushbackCooldown;
    float lastSpellUse;
    public bool shieldActive = false;
    bool loadingMeteor = false;

	// Use this for initialization
	void Start () {
        lastSpellUse = Time.time;
        playerScore = new int[4];
        god.onMeteor += makeHellRain;
        pushbackCooldown = Time.time;
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        //Check respawn
        for (int i = 0; i < Players.Length; i++)
        {
            if (Players[i] != null && Players[i].respawnTimer != 0 && Time.time >= Players[i].respawnTimer && GodHp > 0)
            {
                Players[i].Respawn();
                updatePlayerHealth(Players[i].CtrlId, Players[i].Health);
            }
        }
        
        //God regens mana after not using spells for a while
        if (GodMana < GodMaxMana && Time.time >= lastSpellUse + 2)
            godRegenMana();

        checkGodInput();
	}
    void checkGodInput()
    {
        if (god.IsUsingShield && !shieldActive)
        {
            if(Time.time>pushbackCooldown)
                godKnockBackPlayers();

            shieldActive = true;
            lastSpellUse = Time.time;
        }
        else if (!god.IsUsingShield && shieldActive)
        {
            shieldActive = false;
        }
        if (god.IsUsingShield)
            godUseMana(shieldManaCost * Time.deltaTime);

        if (god.MeteorLoading && !loadingMeteor)
        {
            audio.clip = clips[1];
            audio.Play();
            loadingMeteor = true;
            lastSpellUse = Time.time;
        }
        else if (!god.MeteorLoading && loadingMeteor)
        {
            loadingMeteor = false;
        }
        if (god.MeteorLoading)
            godUseMana(meteorManaCost * Time.deltaTime);
    }
    public void awardPlayerKill(int CtrlId)
    {
        playerScore[CtrlId - 1] = playerScore[CtrlId - 1] + 1;

        hud.SetPlayersScore(playerScore);
    }
    public void updatePlayerHealth(int pid, int hp)
    {
         hud.SetPlayerLife(getPlayerHudPos(pid), hp/100f);
    }
    int getPlayerHudPos(int pid)
    {
        if (pid == 2)
            pid = 3;
        else if (pid == 3)
            pid = 2;
        else if (pid == 4)
            pid = 4;
        return pid - 1;
    }
    void hurtGod(float dmg)
    {
        GodHp -= dmg;
        if (GodHp <= 0)
        {
            endGame();
        }
        hud.SetBossLife(GodHp/GodMaxHp);
    }
    void godUseMana(float amount)
    {
        GodMana -= amount;
        if (GodMana <= 0)
            hurtGod(amount);
        hud.SetBossMana(GodMana/GodMaxMana);
    }
    void godRegenMana()
    {
        GodMana += Time.deltaTime * GodRegenMana;
        hud.SetBossMana(GodMana / GodMaxMana);
    }
    bool canAttackGod(int player)
    {
        for (int i = 0; i < 4; i++)
        {
            if (playerScore[player-1] < playerScore[i])
                return false;
        }

        return !shieldActive;
    }
    public void playerAttackGod(int player, int dmg)
    {
        if (canAttackGod(player))
            hurtGod(dmg);
    }
    void endGame() {
    }
    void godCreateBrick()
    {
        for (int i = 0; i < blockList.Length; i++)
        {
            MultiplePooled.GetPooledObjectByName("BrickSpawn", blockList[i].transform.position);
        }
    }
    void godKnockBackPlayers()
    {
        audio.clip = clips[0];
        audio.Play();
        pushbackCooldown = Time.time + 5;
        int minDistance = 5;
        int force = 10;
        for (int i = 0; i < 4; i++)
        {
            float dist = Vector2.Distance(meteorGodPosition, Players[i].transform.position);
            if (dist == 0)
                dist = 0.01f;

            if (dist > minDistance)
                continue;

            dist = 1 / dist * force;
            Debug.Log(dist);

            Vector2 dir = (Vector2)Players[i].transform.position - (Vector2)meteorGodPosition;
            Players[i].rigidbody2D.velocity = (dir * dist);
            Players[i].stun(.5f);
        }
    }
    void makeHellRain(float time)
    {
        int n = Mathf.Max(1, (int)time * 2);
        MeteorCtrl[] meteors = new MeteorCtrl[n];
        Vector2 randDirection;
        float x = meteorGodPosition.x;
        float y = meteorGodPosition.y;

        for (int i = 0; i < n; i++)
        {
            float nx = x + Random.Range((float)(-meteorRainRadius), (float)(meteorRainRadius));
            float ny = y + Random.Range((float)(-meteorRainRadius), (float)(meteorRainRadius));
            meteors[i] = MultiplePooled.GetPooledObjectByName("Meteor", meteorOrigin).GetComponent<MeteorCtrl>();
            randDirection = new Vector2(nx, ny);
            meteors[i].init(randDirection);
        }
        
        Debug.Log(time);
    }
}