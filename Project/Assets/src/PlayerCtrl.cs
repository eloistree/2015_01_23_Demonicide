﻿using UnityEngine;
using System.Collections;

public class PlayerCtrl : Life
{
    string[] weaponList = { "Sword", "Bow", "Pickaxe" };
    public int CtrlId;
    public int moveSpeed = 10;
    public float attackSpeed = 1;
    public int attackLength = 20;
    public int attackDamage = 25;
    public int respawnTime = 5;
    public int arrowSpeed = 25;
    public int arrowDamage = 15;
    public float dashMultiplier = 4f;
    public float dashDuration = 0.1f;
    public float respawnTimer;
    public tk2dSpriteAnimation animationSet;
    public TeamManager manager;
    public AudioClip[] Clips;

    string weapon = "Bow";
    int weaponId = 1;
    bool isDashing = false;
    public bool hasShield = false;
    bool isDead = false;
    bool isStun = false;
    float stunTime;

    ControllerToPlayerInfo ctrlInfo;
    Vector2 orientation;
    float lastAttack;
    Vector2 spawnPoint;
    tk2dSpriteAnimator anim;
    tk2dSprite sprite;
    float dashTimer;
    public AudioSource audio;

    void Start()
    {
        lastAttack = Time.time;
        orientation = Vector2.up;
        respawnTimer = 0;
        isDashing = false;
        ctrlInfo = GetComponent<ControllerToPlayerInfo>() as ControllerToPlayerInfo;
        anim = GetComponent<tk2dSpriteAnimator>() as tk2dSpriteAnimator;
        audio = GetComponent<AudioSource>() as AudioSource;
        anim.Library = animationSet;
        sprite = GetComponent<tk2dSprite>();
        sprite.color = getPlayerColor();
        anim.Play("FrontSwordIdle");
        stunTime = Time.time;

        spawnPoint = this.transform.position;
        ctrlInfo.onPlayerStartDashing += startDash;
        
    }
    void startDash(int player) {
        if (player != CtrlId)
            return;
        isDashing = true;
        dashTimer = Time.time;
        audio.clip = Clips[6];
        audio.Play();
    }

    void Update()
    {
        if (Time.time > dashTimer + dashDuration)
            isDashing = false;
        if (Time.time > stunTime)
            isStun = false;

    }
    public void stun(float f)
    {
        isStun = true;
        stunTime = Time.time + f;
    }
    public override void HasBeenDammaged(int old, int newl)
    {
        manager.updatePlayerHealth(CtrlId, newl);
        if (old > newl)
        {
            audio.clip = Clips[1];
            audio.Play();
        }
        else
        {
            audio.clip = Clips[4];
            audio.Play();
        }
        if (newl == 0)
        {
            respawnTimer = Time.time + respawnTime;
            isDead = true;
            anim.Play("Ded");
        }
    }
    public void Respawn()
    {
        isDead = false;
        this.transform.position = spawnPoint;
        this.rigidbody2D.MovePosition(spawnPoint);
        Health = 100;
        respawnTimer = 0;
        audio.clip = Clips[4];
        audio.Play();
    }

    void checkInput()
    {
        if (isDead || isStun)
            return;
        if (ctrlInfo.IsPlayerHitting(CtrlId))
        {
            weapon = "Sword";
            attack();
        }
        else if (ctrlInfo.IsPlayerBowing(CtrlId))
        {
            weapon = "Bow";
            attack();
        }
        else if (ctrlInfo.IsPlayerPickaxe(CtrlId))
        {
            weapon = "Pickaxe";
            attack();
        }
        checkMovement();
    }
    void FixedUpdate()
    {
        checkInput();
    }
    void updateAnimation(bool moving)
    {
        if (lastAttack + attackSpeed/2 > Time.time)
            return;

        string animName = "";
        if (orientation == Vector2.up)
            animName += "Back";
        else if (orientation == -Vector2.up)
            animName += "Front";
        else
            animName += "Side";

        animName += weapon == "Pickaxe" ? "Pickaxe" : "Sword";
        if (moving)
            animName += "Walk";
        else
            animName += "Idle";

        anim.Play(animName);
    }
    void playAttackAnimation()
    {
        string animName = "";
        if (orientation == Vector2.up)
            animName += "Back";
        else if (orientation == -Vector2.up)
            animName += "Front";
        else
            animName += "Side";

        animName += weapon == "Pickaxe" ? "Pickaxe" : "Sword";
        animName += "Attack";

        anim.Play(animName);
    }
    void checkMovement()
    {
        Vector2 v = ctrlInfo.PlayerDirection(CtrlId);
        float speed = moveSpeed;
        if (isDashing)
            speed *= dashMultiplier;

        //Move player
        this.rigidbody2D.velocity = new Vector2(speed * v.x, speed * v.y);

        if (v.x == 0 && v.y == 0) {// If no movement, no orientation chnage
            updateAnimation(false);
            return;
        }
        updateAnimation(true);

        if (Mathf.Abs(v.x) > Mathf.Abs(v.y))
        {
            if (v.x > 0){
                orientation = Vector2.right;
                this.transform.localScale = new Vector3(1, 1, 1);

            }
            else{
                orientation = -Vector2.right;
                this.transform.localScale = new Vector3(-1, 1, 1);

            }
        }
        else
        {
            if (v.y > 0)
            {
                orientation = Vector2.up;
                this.transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                orientation = -Vector2.up;
                this.transform.localScale = new Vector3(1, 1, 1);
            }
        }

    }
    void attack()
    {
        if (lastAttack + attackSpeed > Time.time)
            return;
        lastAttack = Time.time;
        playAttackAnimation();
        if (weapon == "Sword" || weapon == "Pickaxe")
        {
            RaycastHit2D hit = Physics2D.Raycast(this.rigidbody2D.position, orientation, attackLength);
            if (hit.collider != null)
            {
                Debug.Log(hit.collider.name);
                if (hit.collider.name == "GodVisual")
                {
                    int dmg = weapon == "Sword" ? attackDamage : (int)(attackDamage / 2);
                    manager.playerAttackGod(CtrlId, dmg/2);
                }
                Life player = hit.collider.gameObject.GetComponent<Life>() as Life;
                if (player != null)
                {
                    if (player is PlayerCtrl)
                    {
                        if (canHitPlayer(player as PlayerCtrl) && !(player as PlayerCtrl).hasShield && !(player as PlayerCtrl).isDead)
                        {
                            player.Health -= weapon == "Sword" ? attackDamage : attackDamage / 2;
                            if (player.Health <= 0)
                                manager.awardPlayerKill(CtrlId);
                        }
                    }
                    else
                    {
                        player.Health -= weapon == "Sword" ? attackDamage / 2 : attackDamage;
                    }
                }
            }
            else if (hit.collider == null && weapon == "Pickaxe")
            {
                Vector2 pos = transform.position;
                pos += orientation * attackLength;
                MultiplePooled.GetPooledObjectByName("BrickSpawn", pos);
                playAttackAnimation();
                audio.clip = Clips[5];
                audio.Play();
            }
        }
        if (weapon == "Bow")
        {
            var arr = MultiplePooled.GetPooledObjectByName("Arrow", transform);
            ArrowCtrl arrow = arr.GetComponent<ArrowCtrl>() as ArrowCtrl;
            if (arrow != null)
            {
                arrow.setDirection(this, orientation, arrowSpeed, arrowDamage, 12);
                audio.clip = Clips[0];
                audio.Play();
            }
        }
    }
    public void switchWeapon(string weap) {
        weapon = weap;
    }
    public bool canHitPlayer(PlayerCtrl plr)
    {
        return true;
    }
    Color getPlayerColor()
    {
        if (CtrlId == 1)
            return Color.red;
        if (CtrlId == 2)
            return Color.yellow;
        if (CtrlId == 3)
            return Color.blue;
        if (CtrlId == 4)
            return Color.green;

        return Color.white;
    }
    public void SetShield(bool s)
    {
        if (s)
        {
            audio.clip = Clips[3];
            audio.Play();
        }
        hasShield = s;
    }
}
