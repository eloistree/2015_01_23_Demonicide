﻿using UnityEngine;
using System.Collections;

public class ArrowCtrl : MonoBehaviour {

    public int damage;
    float dieTimer;
    PlayerCtrl Parent;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (dieTimer < Time.time)
            this.gameObject.SetActive(false);
	    
	}
    public void setDirection(PlayerCtrl par, Vector2 dir, int speed, int dmg, int longevity)
    {
        Parent = par;
        dieTimer = Time.time + longevity;

        if (dir == Vector2.up || dir == -Vector2.up)
        {
            Quaternion rot = new Quaternion();
            if(dir == Vector2.up)
                rot.eulerAngles = new Vector3(0, 0, 90);
            else
                rot.eulerAngles = new Vector3(0, 0, -90);
            transform.rotation = rot;
        }
        if (dir == -Vector2.right)
            this.transform.localScale = new Vector3(-1, 1, 1);
        this.rigidbody2D.velocity = dir * speed;
        damage = dmg;
    }
    void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.collider != null)
        {
            Life player = collider.collider.gameObject.GetComponent<Life>() as Life;
            if (player != null && player != Parent)
            {
                PlayerCtrl plr = player as PlayerCtrl;
                if (plr != null && Parent.canHitPlayer(plr) && !plr.hasShield)
                {
                    player.Health -= damage;
                    this.gameObject.SetActive(false);
                }

                this.rigidbody2D.velocity = Vector2.zero;
                this.collider2D.enabled = false;
            }
        }
    }

    public void OnDisable()
    {
        this.collider2D.enabled =true;
    }
}
