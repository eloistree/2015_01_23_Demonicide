﻿using UnityEngine;
using System.Collections;

public class ActiveVisualShield : MonoBehaviour {

    public TeamManager manager;
    public Transform toDeactivate;
	// Use this for initialization
	void Start () {
        manager = GetComponent<TeamManager>() as TeamManager;
	}
	
	// Update is called once per frame
	void Update () {
        if (manager != null && toDeactivate!=null)
        {
            toDeactivate.gameObject.SetActive(manager.shieldActive);
        }
	
	}
}
