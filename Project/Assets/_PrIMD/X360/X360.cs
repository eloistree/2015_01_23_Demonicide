﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;


public class X360 : MonoBehaviour
{


    public enum ButtonType { A, B, X, Y, Menu, Start, LeftButton, RightButton, PullLeftJoystick, PullRightJoystick, LeftTriggerActive, RightTriggerActive, LeftJoystickActive, RightJoystickActive, ArrowActive }
    public enum Axis { TriggerLeft, TriggerRight, JoystickLeftHorizontal, JoystickLeftVertical, JoystickRightHorizontal, JoystickRightVertical, ArrowHorizontal, ArrowVertical }
    public enum Direction { Left, Right, Arrow }
    public enum Player { All, P1, P2, P3, P4 }
    public static X360 InstanceInScene;
    public void OnEnable()
    {
        InstanceInScene = this;
    }

    public static bool IsActive(ButtonType buttonType, Player player= Player.All)
    {
        X360ControllerData control = GetController( player);
        if(control==null){
            Debug.LogError("No controller define");
            return false;
        }

        switch (buttonType)
        {
            case ButtonType.A: return control.A();
            case ButtonType.B: return control.B();
            case ButtonType.X: return control.X();
            case ButtonType.Y: return control.Y();
            case ButtonType.Menu: return control.Menu();
            case ButtonType.Start: return control.Start();
            case ButtonType.LeftButton: return control.LeftButton();
            case ButtonType.RightButton: return control.RightButton();
            case ButtonType.PullLeftJoystick: return control.LeftJoystickButton();
            case ButtonType.PullRightJoystick: return control.RightJoystickButton();
            case ButtonType.LeftTriggerActive: return control.IsLeftTriggerActive();
            case ButtonType.RightTriggerActive: return control.IsRightTriggerActive();
            case ButtonType.LeftJoystickActive: return control.IsLeftJoystickActive();
            case ButtonType.RightJoystickActive: return control.IsRightJoystickActive();
            case ButtonType.ArrowActive: return control.IsArrowActive();
            default:
                Debug.LogError("Should not be reach"); break;

        }
        return false;


    }

    public static float GetAxe(Axis axe, Player player = Player.All)
    {
        X360ControllerData control = GetController(player);
        if (control == null)
        {
            Debug.LogError("No controller define");
            return 0f;
        }

        switch (axe)
        {
            case Axis.TriggerLeft: return control.GetLeftTrigger();
            case Axis.TriggerRight: return control.GetRightTrigger();

            case Axis.ArrowHorizontal: return control.GetArrows().x;
            case Axis.ArrowVertical: return control.GetArrows().y;

            case Axis.JoystickLeftHorizontal: return control.GetLeft().x;
            case Axis.JoystickLeftVertical: return control.GetLeft().y;

            case Axis.JoystickRightHorizontal: return control.GetRight().x;
            case Axis.JoystickRightVertical: return control.GetRight().y;
            default:
                Debug.LogError("Should not be reach"); break;
        }
        return 0f;
    }

    public static Vector2 GetDirection(Direction direction, Player player = Player.All)
    {
        X360ControllerData control = GetController(player);
        if (control == null)
        {
            Debug.LogError("No controller define");
            return Vector2.zero;
        }
        switch (direction)
        {
            case Direction.Arrow: return control.GetArrows();
            case Direction.Left: return control.GetLeft();
            case Direction.Right: return control.GetRight();
            default:
                Debug.LogError("Should not be reach"); break;
        }
        return Vector2.zero;
    }


    private static X360ControllerData GetController(Player player)
    {
        X360 accessInfo = GetAccessInfo();
        if (InstanceInScene == null)
        {
            Debug.LogWarning("Impossible to acces to X360 Controller, there is none in the scene");
            return null;
        }


        X360ControllerData control = accessInfo.allController;
        if (accessInfo.controllers.Length != 4) { Debug.LogError("Controler Should be initalized with 4 players configuration"); return null; }
        switch (player)
        {
            case Player.P1: control = accessInfo.controllers[0]; break;
            case Player.P2: control = accessInfo.controllers[1]; break;
            case Player.P3: control = accessInfo.controllers[2]; break;
            case Player.P4: control = accessInfo.controllers[3]; break;
        }
        return control;
    }

    private static X360 GetAccessInfo()
    {
        if (InstanceInScene == null)
        {
            Debug.LogWarning("No X360 Data Access found in scene: Creation of default one");
            GameObject x360Access = new GameObject();
            x360Access.name = "X360 Access";
            X360 dataAccess = x360Access.AddComponent<X360>() as X360;
            if (dataAccess)
                InstanceInScene = dataAccess;


        }
        return InstanceInScene;
    }


    public X360ControllerData allController = new X360ControllerData();
    public X360ControllerData[] controllers = new X360ControllerData[] {
        new X360ControllerData{
            _A = KeyCode.Joystick1Button0,
            _B = KeyCode.Joystick1Button1,
            _X = KeyCode.Joystick1Button2,
            _Y = KeyCode.Joystick1Button3,


            _LeftButton = KeyCode.Joystick1Button4,
            _RightButton = KeyCode.Joystick1Button5,
            _LeftJoystickButton = KeyCode.Joystick1Button8,
            _RightJoystickButton = KeyCode.Joystick1Button9,

            _Menu = KeyCode.Joystick1Button6,
            _Start = KeyCode.Joystick1Button7,

            _BothTrigger = "P1 3rd axis triggers",
            _LeftTrigger = "P1 9rd axis left trigger",
            _RightTrigger = "P1 10rd axis right trigger",


            _LeftJoystickHorizontal = "P1 X axis",
            _LeftJoystickVertical = "P1 Y axis",

            _RightJoystickHorizontal = "P1 4th axis",
            _RightJoystickVertical = "P1 5th axis",

            _ArrowHorizontal = "P1 6th axis",
            _ArrowVertical = "P1 7th axis",
        
            _BothTriggerP1 = "P1 3rd axis triggers",
            _LeftTriggerP1 = "P1 9rd axis left trigger",
            _RightTriggerP1 = "P1 10rd axis right trigger"
        },new X360ControllerData{
            _A = KeyCode.Joystick2Button0,
            _B = KeyCode.Joystick2Button1,
            _X = KeyCode.Joystick2Button2,
            _Y = KeyCode.Joystick2Button3,


            _LeftButton = KeyCode.Joystick2Button4,
            _RightButton = KeyCode.Joystick2Button5,
            _LeftJoystickButton = KeyCode.Joystick2Button8,
            _RightJoystickButton = KeyCode.Joystick2Button9,

            _Menu = KeyCode.Joystick2Button6,
            _Start = KeyCode.Joystick2Button7,

            _BothTrigger = "P2 3rd axis triggers",
            _LeftTrigger = "P2 9rd axis left trigger",
            _RightTrigger = "P2 10rd axis right trigger",


            _LeftJoystickHorizontal = "P2 X axis",
            _LeftJoystickVertical = "P2 Y axis",

            _RightJoystickHorizontal = "P2 4th axis",
            _RightJoystickVertical = "P2 5th axis",

            _ArrowHorizontal = "P2 6th axis",
            _ArrowVertical = "P2 7th axis",
        
            _BothTriggerP1 = "P2 3rd axis triggers",
            _LeftTriggerP1 = "P2 9rd axis left trigger",
            _RightTriggerP1 = "P2 10rd axis right trigger"
        }
        ,new X360ControllerData{
            _A = KeyCode.Joystick3Button0,
            _B = KeyCode.Joystick3Button1,
            _X = KeyCode.Joystick3Button2,
            _Y = KeyCode.Joystick3Button3,


            _LeftButton = KeyCode.Joystick3Button4,
            _RightButton = KeyCode.Joystick3Button5,
            _LeftJoystickButton = KeyCode.Joystick3Button8,
            _RightJoystickButton = KeyCode.Joystick3Button9,

            _Menu = KeyCode.Joystick3Button6,
            _Start = KeyCode.Joystick3Button7,

            _BothTrigger = "P3 3rd axis triggers",
            _LeftTrigger = "P3 9rd axis left trigger",
            _RightTrigger = "P3 10rd axis right trigger",


            _LeftJoystickHorizontal = "P3 X axis",
            _LeftJoystickVertical = "P3 Y axis",

            _RightJoystickHorizontal = "P3 4th axis",
            _RightJoystickVertical = "P3 5th axis",

            _ArrowHorizontal = "P3 6th axis",
            _ArrowVertical = "P3 7th axis",
        
            _BothTriggerP1 = "P3 3rd axis triggers",
            _LeftTriggerP1 = "P3 9rd axis left trigger",
            _RightTriggerP1 = "P3 10rd axis right trigger"
        },new X360ControllerData{
            _A = KeyCode.Joystick4Button0,
            _B = KeyCode.Joystick4Button1,
            _X = KeyCode.Joystick4Button2,
            _Y = KeyCode.Joystick4Button3,


            _LeftButton = KeyCode.Joystick4Button4,
            _RightButton = KeyCode.Joystick4Button5,
            _LeftJoystickButton = KeyCode.Joystick4Button8,
            _RightJoystickButton = KeyCode.Joystick4Button9,

            _Menu = KeyCode.Joystick4Button6,
            _Start = KeyCode.Joystick4Button7,

            _BothTrigger = "P4 3rd axis triggers",
            _LeftTrigger = "P4 9rd axis left trigger",
            _RightTrigger = "P4 10rd axis right trigger",


            _LeftJoystickHorizontal = "P4 X axis",
            _LeftJoystickVertical = "P4 Y axis",

            _RightJoystickHorizontal = "P4 4th axis",
            _RightJoystickVertical = "P4 5th axis",

            _ArrowHorizontal = "P4 6th axis",
            _ArrowVertical = "P4 7th axis",
        
            _BothTriggerP1 = "P4 3rd axis triggers",
            _LeftTriggerP1 = "P4 9rd axis left trigger",
            _RightTriggerP1 = "P4 10rd axis right trigger"
        }
    };






}
[System.Serializable]
public class X360ControllerData
{
    public KeyCode _A = KeyCode.JoystickButton0;
    public KeyCode _B = KeyCode.JoystickButton1;
    public KeyCode _X = KeyCode.JoystickButton2;
    public KeyCode _Y = KeyCode.JoystickButton3;


    public KeyCode _LeftButton = KeyCode.JoystickButton4;
    public KeyCode _RightButton = KeyCode.JoystickButton5;
    public KeyCode _LeftJoystickButton = KeyCode.JoystickButton8;
    public KeyCode _RightJoystickButton = KeyCode.JoystickButton9;

    public KeyCode _Menu = KeyCode.JoystickButton6;
    public KeyCode _Start = KeyCode.JoystickButton7;

    public string _BothTrigger = "3rd axis triggers";
    public string _LeftTrigger = "9rd axis left trigger";
    public string _RightTrigger = "10rd axis right trigger";


    public string _LeftJoystickHorizontal = "X axis";
    public string _LeftJoystickVertical = "Y axis";

    public string _RightJoystickHorizontal = "4th axis";
    public string _RightJoystickVertical = "5th axis";

    public string _ArrowHorizontal = "6th axis";
    public string _ArrowVertical = "7th axis";

    public string _BothTriggerP1 = "3rd axis triggers";
    public string _LeftTriggerP1 = "9rd axis left trigger";
    public string _RightTriggerP1 = "10rd axis right trigger";





    public bool A()
    {
        return Input.GetKey(_A) || Input.GetKeyDown(_A);
    }

    public bool B()
    {
        return Input.GetKey(_B) || Input.GetKeyDown(_B);
    }

    public bool X()
    {
        return Input.GetKey(_X) || Input.GetKeyDown(_X);
    }

    public bool Y()
    {
        return Input.GetKey(_Y) || Input.GetKeyDown(_Y);
    }

    public bool LeftButton()
    {
        return Input.GetKey(_LeftButton) || Input.GetKeyDown(_LeftButton);
    }

    public bool RightButton()
    {
        return Input.GetKey(_RightButton) || Input.GetKeyDown(_RightButton);
    }

    public bool Menu()
    {
        return Input.GetKey(_Menu) || Input.GetKeyDown(_Menu);
    }

    public bool Start()
    {
        return Input.GetKey(_Start) || Input.GetKeyDown(_Start);
    }

    public float GetLeftTrigger()
    {
        float axis = 0f;
        try
        {
            axis = Input.GetAxis(_LeftTrigger);
        }
        catch (UnityException e)
        {
            Debug.LogWarning(GetAxeWarning(_LeftTrigger) + e);
        }
        return axis;
    }

    public bool IsLeftTriggerActive()
    {
        return 0f != GetLeftTrigger();
    }

    public float GetRightTrigger()
    {
        float axis = 0f;
        try
        {
            axis = Input.GetAxis(_RightTrigger);
        }
        catch (UnityException e)
        {
            Debug.LogWarning(GetAxeWarning(_RightTrigger) + e);
        }
        return axis;
    }

    public bool IsRightTriggerActive()
    {
        return 0f != GetLeftTrigger();
    }

    public bool LeftJoystickButton()
    {
        return Input.GetKey(_LeftJoystickButton) || Input.GetKeyDown(_LeftJoystickButton);
    }

    public bool RightJoystickButton()
    {
        return Input.GetKey(_RightJoystickButton) || Input.GetKeyDown(_RightJoystickButton);
    }

    public Vector2 GetLeft()
    {

        return GetAxisVector(_LeftJoystickHorizontal, _LeftJoystickVertical);
    }

    public Vector2 GetRight()
    {

        return GetAxisVector(_RightJoystickHorizontal, _RightJoystickVertical);
    }

    public Vector2 GetArrows()
    {


        return GetAxisVector(_ArrowHorizontal, _ArrowVertical);

    }

    private Vector2 GetAxisVector(string horizontal, string vertical)
    {
        Vector2 axis = Vector3.zero;
        try
        {
            axis.x = Mathf.Clamp(Input.GetAxis(horizontal), -1f, 1f);
        }
        catch (UnityException e)
        {
            Debug.LogWarning(GetAxeWarning(horizontal) + e);
        }
        try
        {
            axis.y = Mathf.Clamp(Input.GetAxis(vertical), -1f, 1f);
        }
        catch (UnityException e)
        {
            Debug.LogWarning(GetAxeWarning(vertical) + e);
        }
        return axis;
    }

    public bool IsLeftJoystickActive()
    {
        return GetLeft() != Vector2.zero;
    }

    public bool IsRightJoystickActive()
    {

        return GetRight() != Vector2.zero;
    }
    public bool IsArrowActive()
    {

        return GetArrows() != Vector2.zero;
    }




    private static string GetAxeWarning(string axe)
    {
        return "Please define the axe \"" + axe + "\"  in the Unity3D INPUTMANAGER to be able to track this data.";
    }



}