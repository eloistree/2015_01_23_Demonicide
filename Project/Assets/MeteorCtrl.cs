﻿using UnityEngine;
using System.Collections;

public class MeteorCtrl : MonoBehaviour {
    Vector2 direction;
    public int radius = 4;
    public int damage = 35;
    public float speed = 1;
    public float lifeTime = 2.5f;

    bool exploded = false;
    float explodedSince;

    AudioSource audio;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
	}
    public void init(Vector2 dir)
    {
        direction = dir;
    }
	// Update is called once per frame
	void Update () {
        if (exploded && Time.time > explodedSince + lifeTime)
        {
            exploded = false;
            this.gameObject.SetActive(false);
            return;
        }
        if (direction == (Vector2)this.transform.position)
            this.explode();
        this.transform.position = Vector2.MoveTowards(this.transform.position, direction, speed);
	}
    void explode() {
        if (exploded)
            return;
        exploded = true;
        explodedSince = Time.time;
        audio.Play();
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(this.transform.position, radius);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i] != null)
            {
                Life living = hitColliders[i].GetComponent<Life>() as Life;
                if (living != null)
                {
                    living.Health -= damage;
                }
            }
        }
        MultiplePooled.GetPooledObjectByName("SmallExplosion", this.transform);
    }

}
