﻿using UnityEngine;
using System.Collections;

public class MeteorDirection : MonoBehaviour {

    public Vector2 direction;

    public void Start()
    {
        rigidbody2D.AddForce(direction, ForceMode2D.Force);
    }

}
